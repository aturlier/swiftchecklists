//
//  ItemDetailViewController.swift
//  SwiftChecklists
//
//  Created by Axel Turlier on 02/02/2017.
//  Copyright © 2017 Axel Turlier. All rights reserved.
//

import UIKit

class ItemDetailViewController: UITableViewController {
    
    @IBOutlet weak var itemNameTextField: UITextField!
    var delegate : ItemDetailViewControllerDelegate?
    var isEditingItem : Bool = false
    var item : ChecklistItem? = nil
    var itemRow : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (isEditingItem) {
            itemNameTextField.text = item?.text
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        itemNameTextField.becomeFirstResponder()
    }
    
    func done() {
        if (isEditingItem) {
            delegate?.itemDetailViewController(controller: self, didFinishEditingItem: ChecklistItem.init(text: itemNameTextField.text!, checked: (item?.checked)!), forRow: itemRow)
        } else {
            delegate?.itemDetailViewController(controller: self, didFinishAddingItem: ChecklistItem.init(text: itemNameTextField.text!))
        }
    }
    
    @IBAction func didEndOnExit(_ sender: AnyObject) {
        done()
    }
    
    @IBAction func doneButtonPressed(_ sender: AnyObject) {
        done()
    }
    
    
    @IBAction func cancelButtonPressed(_ sender: AnyObject) {
        delegate?.itemDetailViewControllerDidCancel(controller: self)
    }
    
}

extension ListDetailViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.navigationItem.rightBarButtonItem?.isEnabled = listNameTextField.text != ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

protocol ItemDetailViewControllerDelegate : class {
    func itemDetailViewControllerDidCancel(controller: ItemDetailViewController)
    func itemDetailViewController(controller: ItemDetailViewController, didFinishAddingItem item: ChecklistItem)
    func itemDetailViewController(controller: ItemDetailViewController, didFinishEditingItem item: ChecklistItem, forRow row: Int);
}
