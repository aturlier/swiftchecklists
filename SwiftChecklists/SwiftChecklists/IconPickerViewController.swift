//
//  IconPickerViewController.swift
//  SwiftChecklists
//
//  Created by Axel Turlier on 29/03/17.
//  Copyright © 2017 Axel Turlier. All rights reserved.
//

import UIKit

class IconPickerViewController : UITableViewController {
    static let icons = [
        "Appointments",
        "Birthdays",
        "Chores",
        "Drinks",
        "Folder",
        "Groceries",
        "Inbox",
        "No Icon",
        "Photos",
        "Trips"
    ]
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return IconPickerViewController.icons.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "iconCell", for: indexPath)
        cell.imageView?.image = UIImage.init(named: IconPickerViewController.icons[indexPath.row])
        cell.textLabel?.text = IconPickerViewController.icons[indexPath.row]
        return cell
    }
}
