//
//  ViewController.swift
//  SwiftChecklists
//
//  Created by Axel Turlier on 02/02/2017.
//  Copyright © 2017 Axel Turlier. All rights reserved.
//

import UIKit

class ChecklistViewController: UITableViewController {
    
    var checklist : Checklist!
    var shouldPerformSegue : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //loadChecklistItems()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //loadChecklistItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = checklist?.checklistName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Normal Functions
    
    func configureCheckmarkFor(cell: UITableViewCell, withItem item: ChecklistItem) {
        (cell.viewWithTag(10) as! UILabel).text = item.checked ? "✔️": ""
    }
    
    func configureTextFor(cell: UITableViewCell, withItem item: ChecklistItem) {
        (cell.viewWithTag(20) as! UILabel).text = item.text
    }
    
    
    
    // MARK: - TableView DataSource & Delegate
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklist.checklistItems.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item : ChecklistItem = checklist.checklistItems[indexPath.item]
        item.toggleCheck()
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ItemEdit", sender: tableView.cellForRow(at: indexPath))
        shouldPerformSegue = true
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistItem", for: indexPath);
        
        let item : ChecklistItem = checklist.checklistItems[indexPath.item]
        
        configureCheckmarkFor(cell: cell, withItem: item)
        configureTextFor(cell: cell, withItem: item)
        
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.delete) {
            checklist.checklistItems.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
        }
    }
    
    // MARK: - Segues
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == "ItemEdit") {
            return shouldPerformSegue
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller : ItemDetailViewController = (segue.destination as! UINavigationController).viewControllers[0] as! ItemDetailViewController
        controller.delegate = self
        if (segue.identifier == "ItemEdit") {
            shouldPerformSegue = false
            controller.isEditingItem = true
            controller.item = checklist.checklistItems[tableView.indexPath(for: (sender as! UITableViewCell))!.item]
            controller.itemRow = tableView.indexPath(for: (sender as! UITableViewCell))!.item
        }
    }
}


// MARK: - Extensions
extension ChecklistViewController: ItemDetailViewControllerDelegate {
    func itemDetailViewControllerDidCancel(controller: ItemDetailViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func itemDetailViewController(controller: ItemDetailViewController, didFinishAddingItem item: ChecklistItem) {
        controller.dismiss(animated: true) {
            self.checklist.checklistItems.append(item)
            self.tableView.insertRows(at: [IndexPath.init(row: self.checklist.checklistItems.count - 1, section: 0)], with: UITableViewRowAnimation.right)
            //self.saveChecklistItems()
        }
    }
    
    func itemDetailViewController(controller: ItemDetailViewController, didFinishEditingItem item: ChecklistItem, forRow row: Int) {
        controller.dismiss(animated: true) {
            self.checklist.checklistItems[row] = item
            self.tableView.reloadRows(at: [IndexPath.init(row: row, section: 0)], with: UITableViewRowAnimation.fade)
            //self.saveChecklistItems()

        }
    }
}
