//
//  Checklist.swift
//  SwiftChecklists
//
//  Created by Axel Turlier on 02/03/2017.
//  Copyright © 2017 Axel Turlier. All rights reserved.
//

import Foundation

class Checklist : NSObject, NSCoding {
    var checklistItems : [ChecklistItem] = []
    var checklistName : String = ""
    var iconName : String = "No Icon"
    
    var uncheckedItems : Int {
        get {
            return checklistItems.filter{ $0.checked }.count
        }
    }
    
    init(withName name: String, andItems checklistItems : [ChecklistItem] = []) {
        self.checklistItems = checklistItems
        self.checklistName = name
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder decoder: NSCoder) {
        
        guard let checklistItems = decoder.decodeObject(forKey: "checklistItems") as? [ChecklistItem],
        let name = decoder.decodeObject(forKey: "checklistName") as? String,
            let icon = decoder.decodeObject(forKey: "iconName") as? String
            else { return nil }
        
        self.init(withName: name, andItems: checklistItems)
        self.iconName = icon
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.checklistItems, forKey: "checklistItems")
        aCoder.encode(self.iconName, forKey: "iconName")
    }

}
