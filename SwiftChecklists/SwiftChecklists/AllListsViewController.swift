//
//  AllListsViewController.swift
//  SwiftChecklists
//
//  Created by Axel Turlier on 02/03/2017.
//  Copyright © 2017 Axel Turlier. All rights reserved.
//

import UIKit

class AllListsViewController : UITableViewController {
    
    var checklists : [Checklist] = DataModel.sharedInstance.checklists
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "checklist", for: indexPath)
        cell.textLabel?.text = checklists[indexPath.row].checklistName
        let unchecked = checklists[indexPath.row].uncheckedItems
        if (checklists[indexPath.row].checklistItems.count == 0) {
            cell.detailTextLabel?.text = "No Items"
        } else if (unchecked == 0) {
            cell.detailTextLabel?.text = "All Done"
        } else {
            cell.detailTextLabel?.text = String(unchecked)
        }
        cell.imageView?.image = UIImage.init(named: checklists[indexPath.row].iconName)
        return cell
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklists.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "allListsToChecklist") {
            (segue.destination as! ChecklistViewController).checklist = checklists[(self.tableView.indexPathForSelectedRow?.row)!]
        } else if (segue.identifier == "allListsToAddList") {
            ((segue.destination as! UINavigationController).viewControllers[0] as! ListDetailViewController).delegate = self
        }
    }
}

extension AllListsViewController : ListDetailViewControllerDelegate {
    func listDetailViewControllerDidCancel(controller: ListDetailViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func listDetailViewController(controller: ListDetailViewController, didFinishAddingList list: Checklist) {
        controller.dismiss(animated: true) {
            DataModel.sharedInstance.checklists.append(list)
            DataModel.sharedInstance.sortChecklists()
            self.checklists = DataModel.sharedInstance.checklists
            UIView.transition(
                with: self.tableView,
                duration: 0.35,
                options: .transitionCrossDissolve,
                animations: { () -> Void in
                    self.tableView.reloadData()
                },
                completion: nil)
        }
    }
}
