//
//  ListDetailViewController.swift
//  SwiftChecklists
//
//  Created by Axel Turlier on 09/03/2017.
//  Copyright © 2017 Axel Turlier. All rights reserved.
//

import UIKit

class ListDetailViewController : UITableViewController {
    
    @IBOutlet weak var listNameTextField: UITextField!
    @IBOutlet weak var iconImageView: UIImageView!
    var delegate : ListDetailViewControllerDelegate? = nil
    
    override func viewDidLoad() {
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        listNameTextField.becomeFirstResponder()
    }
    
    func done() {
        delegate?.listDetailViewController(controller: self, didFinishAddingList: Checklist.init(withName: listNameTextField.text!))
    }
    
    @IBAction func didPressCancel(_ sender: AnyObject) {
        delegate?.listDetailViewControllerDidCancel(controller: self)
    }

    @IBAction func didPressDone(_ sender: AnyObject) {
        done()
    }
    
    
    
}

extension ItemDetailViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.navigationItem.rightBarButtonItem?.isEnabled = itemNameTextField.text != ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

protocol ListDetailViewControllerDelegate : class {
    func listDetailViewControllerDidCancel(controller: ListDetailViewController)
    func listDetailViewController(controller: ListDetailViewController, didFinishAddingList list: Checklist)
}
