//
//  DataModel.swift
//  SwiftChecklists
//
//  Created by Axel Turlier on 09/03/2017.
//  Copyright © 2017 Axel Turlier. All rights reserved.
//

import Foundation

class DataModel {
    static let sharedInstance = DataModel()
    var checklists : [Checklist] = []
    
    private init() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(DataModel.saveChecklists),
            name: .UIApplicationDidEnterBackground,
            object: nil)
    }
    
    //MARK :- Save / Load data
    func documentDirectory() -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    func dataFileUrl() -> URL {
        let docPath : URL = documentDirectory()
        return docPath.appendingPathComponent("SwiftChecklists.plist")
    }
    
    @objc func saveChecklists() {
        NSKeyedArchiver.archiveRootObject(self.checklists, toFile: dataFileUrl().path)
    }
    
    func loadChecklists() {
        let list = (NSKeyedUnarchiver.unarchiveObject(withFile: dataFileUrl().path) as? [Checklist])
        if (list != nil) {
            self.checklists = list!
        }
    }
    
    func sortChecklists() {
        checklists.sort { $0.checklistName.localizedCompare($1.checklistName) == .orderedAscending }
    }

}
