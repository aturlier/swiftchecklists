//
//  ChecklistItem.swift
//  SwiftChecklists
//
//  Created by Axel Turlier on 02/02/2017.
//  Copyright © 2017 Axel Turlier. All rights reserved.
//

import Foundation

class ChecklistItem : NSObject, NSCoding {
    var text : String = ""
    var checked : Bool = false
    
    init(text: String, checked: Bool = false) {
        self.text = text;
        self.checked = checked;
    }
    
    func toggleCheck() {
        self.checked = !self.checked
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder decoder: NSCoder) {
        
        guard let text = decoder.decodeObject(forKey: "text") as? String
            else { return nil }
        
        let checked = decoder.decodeBool(forKey: "checked")
        
        self.init(text: text, checked: checked)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.text, forKey: "text")
        aCoder.encode(self.checked, forKey: "checked")
    }

}
